#!/bin/bash
# -*- ENCODING: UTF-8 -*-
#En el proceso se instalara apache2 mysql y php 7.1

#Funcion la cual nos ayuda saber si existe un directorio de mysql
mysql_exist () { #gracias a esta funcion sabemos si en el sistema ya esta instalado mysql
  if [ -d /etc/mysql/ ]; then
    return 1
  else
    return 0
  fi
}
#Funcion la cual nos ayuda saber si existe un directorio de apache2
apache_exist () { #gracias a esta funcion sabemos si en el sistema ya esta instalado apache2
  if [ -d /etc/apache2/ ]; then
    return 1
  else
    return 0
  fi 
}
#Funcion la cual nos ayuda saber si existe un directorio de php 7.1
php_7_1_exist () { #gracias a esta funcion sabemos si en el sistema ya esta instalado php 7.1
  if [ -d /etc/php/7.1/ ]; then
    return 1
  else
    return 0
  fi
}
#funcion que establece la instalacion de php 7.1 y sera llamada en install_php
install_php_7_1 () {
    #install php 7.1
    sudo add-apt-repository -y ppa:ondrej/php
    sudo apt-get update
    sudo apt-get install php7.1
    #installation of libraries to php
    sudo apt-get install php7.1-mcrypt php7.1-cli php7.1-xml php7.1-zip php7.1-mysql php7.1-gd php7.1-imagick php7.1-recode php7.1-tidy php7.1-xmlrpc
    #paquete el cual nos ayudara a cambiar de version
    sudo apt-get install libapache2-mod-php7.1
}
#funcion que habilita  unicamente la version 7.1
change_mod_7_1 (){
    sudo a2dismod php7.4 php7.3 php7.2 php7.0 php5.6
    sudo a2enmod php7.1
    sudo systemctl restart apache2
}
#funcion que instala apache
install_apache () {
    if [ $apache = "0" ]; then
    sudo apt install apache2
    else
        key=1
        while [ $key -eq 1 ]
        do
            read -p "ya existe apache en el sistema, desea reinstalarlo (y)/(n) " accept_php
            if [ "$accept_php" = "y" -o "$accept_php" = "s" -o "$accept_php" = "Y" -o "$accept_php" = "S" ]; then
                sudo apt install apache2
                key=0
            elif [ "$accept_php" = "n" -o "$accept_php" = "N" ]; then
                key=0
            else
                echo "caracter no valido"
            fi
        done
    fi
}
#funcion que instala mysql
install_mysql () {
        if [ $mysql = "0" ]; then
        sudo apt-get install mysql-server mysql-client
    else
        key=1
        while [ $key -eq 1 ]
        do
            read -p "ya existe mysql en el sistema, desea reinstalarlo (y)/(n) " accept_php
            if [ "$accept_php" = "y" -o "$accept_php" = "s" -o "$accept_php" = "Y" -o "$accept_php" = "S" ]; then
                sudo apt-get install mysql-server mysql-client
                key=0
            elif [ "$accept_php" = "n" -o "$accept_php" = "N" ]; then
                key=0
            else
                echo "caracter no valido"
            fi
        done
    fi
}
#funcion que instala php
install_php () {
        if [ $php = "0" ]; then
        install_php_7_1
        change_mod_7_1
    else
        key=1
        while [ $key -eq 1 ]
        do
            read -p "ya existe php 7.1 en el sistema, desea reinstalarlo (y)/(n) " accept_php
            if [ "$accept_php" = "y" -o "$accept_php" = "s" -o "$accept_php" = "Y" -o "$accept_php" = "S" ]; then
                install_php_7_1
                key=0
            elif [ "$accept_php" = "n" -o "$accept_php" = "N" ]; then
                key=0
            else
                echo "caracter no valido"
            fi
        done
        change_mod_7_1
    fi
}

key=1	
while [ $key -eq 1 ]
do
    #asignamos variables a los valores de las funciones
    mysql_exist
    mysql=$?
    apache_exist
    apache=$?
    php_7_1_exist
    php=$?
    echo "----------------------------------------------"
    echo "|                   Menu                     |"
    echo "----------------------------------------------"
    echo "| 1.Cambiar la version a php7.1              |"
    echo "| 2.Instalar php7.1                          |"
    echo "| 0.Salir                                    |"
    echo "----------------------------------------------"
    read -p "Que desea realizar: " accept_php
    if [ "$accept_php" = "1" ]; then
        if [ $php = "0" ]; then
            echo "No tienes instalado php7.1"
        else
            change_mod_7_1
            
        fi
    elif [ "$accept_php" = "2" ]; then
        install_apache
        install_mysql
        install_php
        sudo apt update
        sudo apt-get upgrade
    elif [ "$accept_php" = "0" ]; then
        exit
    else
        echo "seleccion no valida"
    fi
done 


