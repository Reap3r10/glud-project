#!/bin/sh
# -*- ENCODING: UTF-8 -*-
#funcion que nos ayuda a instalar el nvm
install_nvm () {
    sudo apt install wget
    wget -qO- https://raw.githubusercontent.com/creationix/nvm/v0.34.0/install.sh | bash
}

key=1
while [ $key -eq 1 ]
do
    echo "-------------------------------------------------"
    echo "|                   Menu                        |"
    echo "-------------------------------------------------"
    echo "| 1.Instalar NVM                                |"
    echo "| 0.Salir                                       |"
    echo "------------------------------------------------"
    echo "|                   Nota                        |"
    echo "------------------------------------------------"
    echo "|Cuando tengas nvm ya instalado ejecuta:        |"
    echo "|             source ~/.profile                 |"
    echo "|Si deseas ver las versiones de Node.js ejecuta:|"
    echo "|               nvm ls-remote                   |"    
    echo "|para instalar la version que deseas ejecuta:   |"
    echo "|           nvm install 'version'               |"
    echo "|Ejemplo: nvm install 11.4.0                    |"
    echo "|Si deseas ver la version instalada ejecuta:    |"
    echo "|                  node -v                      |"
    echo "-------------------------------------------------"
    read -p "Que desea realizar: " accept_nvm
    if [ "$accept_nvm" = "1" ]; then
        install_nvm
    elif [ "$accept_nvm" = "0" ];then
        exit
    else
        echo "Caracter no valido"
    fi
done